package iwein.samples.plumber;

/**
 * @author iwein
 */
public interface Plumber {
    void doStuff();
}
