package iwein.samples.plumber;

import iwein.samples.plumber.Plumber;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author iwein
 */
public class PlumberWrapper implements BeanPostProcessor{


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        //
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Plumber){
            System.out.println("wrapping "+ bean);
        }
        return bean;
    }
}
