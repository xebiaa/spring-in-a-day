package iwein.samples.plumber;

/**
 * @author iwein
 */
public class You {

    private Plumber plumber;

    public You(Plumber plumber) {
        this.plumber = plumber;
    }

    public void doPlumbing(){
        plumber.doStuff();
    }
}
