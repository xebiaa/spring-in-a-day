package iwein.samples.plumber.implementation;

import iwein.samples.plumber.Plumber;

/**
 * @author iwein
 */
public class MarioBrothers implements Plumber {
    @Override
    public void doStuff() {
        throw new UnsupportedOperationException("You should implement this...");
    }
}
