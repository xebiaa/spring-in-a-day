package iwein.samples.plumber;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * @author iwein
 */
public class PlumberReplacer implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        BeanDefinition marioBrothers = beanFactory.getBeanDefinition("marioBrothers");
        marioBrothers.setBeanClassName("Foo");
    }
}
