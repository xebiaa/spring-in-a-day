package iwein.samples;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author iwein
 */
public class SimpleContextLoadingTest {

    @Test
    public void shouldLoadAndDestroyContext() {
        //load a spring context and destroy it
        new ClassPathXmlApplicationContext("simple-context.xml").destroy();
    }

}
