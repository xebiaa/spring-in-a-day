package iwein.samples;

import iwein.samples.plumber.You;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author iwein
 */
@ContextConfiguration(locations = "classpath:factory-postprocessing-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class FactoryPostProcessingTest {

	@Autowired
	You you;

	@Test
	public void shouldLoadContext() throws Exception {
		// spring does the job
		Assert.assertNotNull(you);
	}

}
